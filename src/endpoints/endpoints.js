export const LOGIN_URL = "http://localhost:4000/api/customers/login";
export const REGISTRATION_URL = "http://localhost:4000/api/customers";
export const SUBSCRIBE_URL = "http://localhost:4000/api/subscribers";
export const GET_PRODUCTS_URL = "http://localhost:4000/api/products";
export const GET_FAVORITES = "http://localhost:4000/api/customers/customer";
export const NEW_CART_URL = "http://localhost:4000/api/cart";
export const MAKE_ORDERS = "http://localhost:4000/api/orders";
